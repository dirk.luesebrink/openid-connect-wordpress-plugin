#!/bin/bash

function e {
    fn=$1
    tests=$(find tests/Unit -iname $(basename ${fn%.*})Test.php -o -name $(basename ${fn%.*}).php)
    echo ${tests:-tests}
} 

while true; do
    #FILE=$(inotifywait --recursive --exclude=".*.*sw*"  ./app ./tests  --format "%w%f" -e close_write) &&
    #fswatch  -e ".*" -i "\\.php$" --event=Updated app tests
    FILE=$(fswatch -1  -e ".*" -i "\\.php$" --event=Updated --event=MovedTo tests src *.php)
    clear
    #./vendor/bin/sail artisan test
    echo "changed: $FILE"
    tests=$(e $FILE)
    echo "tests: ${tests}"

    echo "run: ./vendor/bin/phpunit --testdox $tests"
    ./vendor/bin/phpunit --testdox $tests
done
