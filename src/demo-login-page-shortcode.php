<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

// register shortcat in plugin namespace but use closure to avoid string
// operations to get the function NAMESPACE right
add_shortcode('healy-oidc-demo-login-page', fn() => render_demo_login_page(...func_get_args()));

function render_demo_login_page($atts = [], ?string $content = null, string $tag = ''): string
{
    $html = file_get_contents(__DIR__ . '/../demo-login-page.html');
    return do_shortcode($html);
}
