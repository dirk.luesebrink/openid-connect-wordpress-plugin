<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

add_shortcode('healy-oidc-login-status', function ($atts = [], ?string $content = null, string $tag = ''): string {
    $user = wp_get_current_user();
    if (empty($user) || $user->ID === 0) {
        return render_not_logged_in_user();
    }
    return render_logged_in_user($user);
});

function render_not_logged_in_user(): string
{
    return "<p>You are not logged in. On successful login, you will find your status here.</p>";
}

function render_logged_in_user(\WP_User $user): string
{
    [$username, $email] = [$user->user_login ?? '?', $user->user_email ?? '?'];
    $logout_url = '/wp-login.php?action=logout';
    return "
        <p>You are logged as:<li><le>username: {$username}</li><li>email: {$email}</le></li></p>
        <p>Start again by <a href='{$logout_url}'>logging out.</a></p>
    ";
}
