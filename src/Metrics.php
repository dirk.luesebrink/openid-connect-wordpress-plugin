<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

use Healy\OpenIdConnect\Plugin\Counter;
use Healy\OpenIdConnect\Plugin\Error;

final class Metrics
{
    const PLUGIN_KEY = 'healy-openid-connect-metrics';
    const USER_KEY = 'healy-openid-connect-user-metrics';

    // well-known predefined metric keys, BUT you can choose any string you like...
    //
    // This is bit unforunate, but relying on wordpress here also means using
    // their generic database design, which, no surprise, uses strings for
    // key/value pair key types. I would have prepared int's and maybe an php8
    // backed enum here, but that is not available.
    //
    const AUTO_REGISTERED_USERS    = "auto-registered-users";
    const BLOCKED_REQUEST          = "blocked-request";
    const COMPLETED_LOGON_REQUESTS = "completed-logon-requests";
    const ERRONEOUS_REQUESTS       = "erroneous-requests";
    const FAILED_REQUESTS          = "failed-requests";
    const IDENTIFIED_REQUEST       = "identified-request";
    const SUCCESSFUL_LOGINS        = "successful-logins";
    const TOTAL_LOGONS_REQUESTS    = "total-logons-requests";
    const UNIQUE_USERS             = "unique-users";

    private array $values = [];

    // plugin wide counter
    private Counter $plugin_counter;

    // user specific counter
    private Counter $wp_user_counter;
    private int $wp_user_id = -1;

    public function __construct()
    {
        // empty counter for an yet unspecified user (as a null object pattern)
        $this->wp_user_id = -1;
        $this->wp_user_counter = new Counter();

        //$this->plugin_counter = null;

        // reloading both counters
        $this->refresh();
    }

    public function save(): void
    {
        [$plugin_values, $user_values] = $this->values();

        dbg(['saving ' . self::PLUGIN_KEY => $plugin_values]);
        update_option(self::PLUGIN_KEY, $plugin_values, true);

        // null user counter is never saved
        if (0 < $this->wp_user_id) {
            dbg(['saving ' . self::USER_KEY => $user_values]);
            update_user_meta($this->wp_user_id, self::USER_KEY, $user_values);
        }
    }

    public function refresh(int $wp_user_id = -1): void
    {
        $this->plugin_counter = new Counter(get_option(self::PLUGIN_KEY, []));

        if (0 < $wp_user_id) {
            $this->wp_user_id = $wp_user_id;
        }

        $user_values = [];
        if (0 < $this->wp_user_id) {
            $user_values = get_user_meta($this->wp_user_id, self::USER_KEY, true);
            // this line below is to make sense of the bizarr wp return value,
            // which returns an empty string on 'true' for non-existing keys...
            $user_values = is_string($user_values) ? [] : $user_values;
        }
        $this->wp_user_counter = new Counter($user_values);
    }

    public function inc(string $key, int $wp_user_id = -1): Metrics
    {
        $this->plugin_counter->inc($key);

        if (0 < $wp_user_id) {
            $this->user_counter($wp_user_id)->inc($key);
        }

        return $this;
    }

    public function incError(Error $error, int $wp_user_id = -1): Metrics
    {
        $key = "E{$error->code()}";
        return $this->inc($key, $wp_user_id);
    }

    public function dec(string $key, int $wp_user_id = -1): Metrics
    {
        $this->plugin_counter->dec($key);

        if (0 < $wp_user_id) {
            $this->user_counter($wp_user_id)->dec($key);
        }

        return $this;
    }

    public function decError(Error $error, int $wp_user_id = -1): Metrics
    {
        $key = "E{$error->code()}";
        return $this->dec($key, $wp_user_id);
    }

    public function values(): array
    {
        return [$this->plugin_counter->all(), $this->wp_user_counter->all()];
    }

    // returns matching user counter or creates a new one if the current one
    // doesnt match the id.
    private function user_counter(int $wp_user_id): Counter
    {
        if ($this->wp_user_id !== $wp_user_id) {
            $this->wp_user_id = $wp_user_id;

            $values = get_user_meta($wp_user_id, self::USER_KEY, true);
            // this line below is to make sense of the bizarr wp return value,
            // which returns an empty string on 'true' for non-existing keys...
            $values = is_string($values) ? [] : $values;

            $this->wp_user_counter = new Counter($values);
        }

        return $this->wp_user_counter;
    }
}
