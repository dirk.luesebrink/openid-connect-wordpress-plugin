<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

use WP_Http;

use Healy\OpenIdConnect\Plugin\Error;

use function Functional\{contains, each, map, reduce_left as reduce , select, unique};

function log($msg, string $prefix = 'I'): void
{
    if (is_array($msg) || is_object($msg)) {
        $msg = json_encode($msg);
    }

    error_log("{$prefix} {$msg}");
}

function err($msg, string $prefix = 'E'): void
{
    log($msg, $prefix);
}

function dbg($msg, string $prefix = 'D'): void
{
    if ((defined('WP_DEBUG') && WP_DEBUG) && ('production' !== wp_get_environment_type())) {
        log($msg, $prefix);
    }
}

function update_user_roles(\WP_User $user, object $claims, string $access_token = null): void
{
    if (is_untouchable($user)) {
        //[$username, $email] = [$user->user_login ?? '?', $user->user_email ?? '?'];
        //dbg("Untouchable, user role update skipped for: ({$username}|{$email})");
        return;
    }

    // the user filter can add & remove roles or return a complete different list of roles
    $current_roles = $user->roles;
    $new_roles = unique(apply_filters(
        'healy-openid-connect-user-roles', $current_roles, $user, $claims, $access_token
    ));

    // now it is about the delta between the roles the user already had before
    // the filter being called and the ones the filter returns. There are roles
    // which are new, so they are added, and they are roles which are gone, so
    // they are removed from the user.
    //
    // The default case is, there is no filter installed, in which case the
    // role set associated with a user remains unchanged.

    // 1. remove roles which are not longer in the list
    // remove roles from user which are not any longer in its new_roles list
    $remove_roles = array_diff($current_roles, $new_roles);
    // but exclude protected roles, they are never removed
    //dbg(["removing protected roles from role update: " => protected_roles()]);
    $remove_roles = array_diff($remove_roles, protected_roles());
    each($remove_roles, fn($role) => $user->remove_role($role));

    // 2. add new roles
    $add_roles = array_diff($new_roles, $current_roles);
    each($add_roles, fn($role) => $user->add_role($role));
}

function update_user_capabilities(\WP_User $user, object $claims, string $access_token = null): void
{
    $current_caps = array_keys($user->allcaps);
    $new_caps = unique(
        apply_filters(
            'healy-openid-connect-user-capabilities',
            $current_caps,
            $user,
            $claims,
            $access_token
        )
    );

    // remove capability from user which are not any longer in its new_caps list
    $to_be_removed = array_values(array_diff($current_caps, $new_caps));
    each($to_be_removed, fn($capability) => $user->remove_cap($capability));

    // add the ones which are new
    $to_be_added = array_values(array_diff($new_caps, $current_caps));
    each($to_be_added, fn($capability) => $user->add_cap($capability));
}

// update user meta data from claims according to following rules:
//
//  - if there is a mapping for a claim, it is updating the user meta value
//  - if there is no mapping for a claim, the claim is ignored
//  - all keys returned from 'healy-openid-connect-user-meta' filter are
//  updated/added to user-meta
//
function update_user_meta_from_claims(
    \WP_User $user, object $claims, array $mapping, string $access_token = null
): void {

    dbg([
        'claims' => $claims,
        'mapping' => $mapping
    ]);

    // For every mapped claims key, check if there is an actual value in the
    // claims for it, and, if it is, use it to update the mapped user meta data
    // key with it
    $wp_user_id = $user->ID;
    each($mapping, function ($claims_key, $wp_key) use ($wp_user_id, $claims): void {
        if (property_exists($claims, $claims_key)) {
            update_user_meta($wp_user_id, $wp_key, $claims->{$claims_key});
        }
    });

    // This is a filter to programmatically add arbitrary user meta fields to
    // the user. As the filter is called with user, claims and access token
    // context, it might implement complicated logic to pull extra user meta
    // data from any source or rule it likes.
    $to_be_added = apply_filters('healy-openid-connect-user-meta', [], $user, $claims, $access_token);
    each($to_be_added, fn($val, $key) => update_user_meta($wp_user_id, $key, $val));
}

//function claims_mapping(): array
//{
//    return mapped_claims('claims-mapping');
//}

function mapped_claims(string $name): array
{
    return parse_multi_line_claims_format(oidc($name, ''));
}

// claims mapping follows the HTTP header format, e.g:
//
// line: with "quoted" string!
// url: with http://service.com
// sql: select 1 from users;
// html: <em>xxx</em>
// escaped string: \"foo\"
//
// where the claims field is on the left, and the user meta field on the right. For example:
//
// a: A
// b: B
//
// as claims mapping input would return this as mapping array: ['a' => 'A', 'b' => 'B']
//
function parse_multi_line_claims_format(string $claims): array
{
    $mappings = [];

    // this parsing, what a mess...  better check the unit tests to understand
    // what is expected to happen here

    // split mapping into lines and ignoring blank ones
    $non_blank_lines = select(
        preg_split('/\R/', $claims),
        fn($e) => !empty($e)
    );

    // split lines into list of key:value pairs
    $key_values_pairs_list = map(
        $non_blank_lines,
        // map <string> to array<string>|array<string, mixed>: "k:v" -> [k, v], "k" -> [k]
        fn($line) => map(explode(':', $line, 2), fn($e) => trim($e)) // split lines into [$key, $value]
    );

    each(
        $key_values_pairs_list,
        // put [$key, $value] mappings into $mapppings[$key] = $value hash
        function ($mapping) use (&$mappings): void {
            $key = $mapping[0];
            $value = (empty($mapping[1]) ? $key : $mapping[1]);
            $mappings[mb_strtolower($key)] = $value;
        }
        //fn($entry) => $mappings[$entry[0]] = $entry[1] //[$entry[0]] = $entry[t]
    );
    //print_r("++: " . json_encode($mappings));

    return $mappings;
}

function is_untouchable(\WP_User $user): bool
{
    return ! empty(array_intersect($user->roles, untouchable_roles()));
}

function untouchable_roles(): array
{
    return list2array(oidc('untouchable-roles', ''));
}

function protected_roles(): array
{
    return list2array(oidc('protected-roles', ''));
}


// convert a comma, semi-colon or whitepace separated list of strings in to
// an array of trimed strings, e.g.:
//
//  'foo,   bar-laber  gonzo!;  x   " -> ['foo', 'bar-laber', 'gonzo!', 'x']
//
function list2array(string $text = ''): array
{
    return array_values(
        select(
            map(preg_split("/\s*[,; ]\s*/", $text), fn($e) => trim($e)),
            fn($e) => !empty($e)
        )
    );
}

function oidc(string $key, ?string $default = null) // : mixed
{
    $val = oidc_options()[$key] ?? null;
    return empty($val) ? $default : $val;
}
function oidc_options(bool $reload = false): array
{
    static $cache = null;
    if (empty($cache) || $reload) {
        $cache = get_option('healy-openid-connect');
        $cache || ($cache = []);
    }
    return $cache;
}

function is_admin_id_token(object $claims): bool
{
    return in_array('Admin', ($claims->roles ?? []));
}

// returns array of blocking reasons, empty array in case there is none
function blocking_reasons(array $claims): array {
    $blocked = [];

    // blocked by claim?
    $blocking_claims_found = check_blocking_claims($claims);
    if (!empty($blocking_claims_found)) {
        dbg('blocking claims found: ' . json_encode($blocking_claims_found));
        //$blocked = [true, 'user login blocked by claims: ' . json_encode($blocking_claims_found)];
        $blocked[] = new Error(Error::BLOCKED_BY_CLAIMS, ['claims' => $blocking_claims_found]);
    }

    // missing mandatory claim?
    $missing_mandatory_claims = check_mandatory_claims($claims);
    if (!empty($missing_mandatory_claims)) {
        dbg('mandatory claims found missing: ' . json_encode($missing_mandatory_claims));
        //$blocked = [true, 'user is missing mandatory claims for log-in: ' . json_encode($missing_mandatory_claims)];
        $blocked[] = new Error(Error::MISSING_CLAIMS, ['claims' => $missing_mandatory_claims]);
    }

    return $blocked;
}


// complex logic, sorry, but this walks you through this:
//
// - get a list of all blocking claims
// - than for each blocking claim, check if the user claims (the incoming parameter) contains that claim key
// - if so, convert the right hand side of the blocking claim into a list of values
// - check if the user claim value is in that list of blocking claim values
// - if so, add this claim (key,value) to the blocked array
// - in the end return the blocked array
//
// the blocked array contains all user claims which are blocking him from entry.
//
function check_blocking_claims(array $claims): array
{
    // accumulating found claims to be returned as result
    $blocked = [];

    // enforce user claim keys to be lowercase
    $claims = reduce($claims, fn($v, $k, $c, $acc) => array_merge($acc, [mb_strtolower($k) => $v]), []);

    // get the list of all blocking claims
    $blocking_claims = parse_multi_line_claims_format(oidc('blocking-claims', ''));
    //dbg(['blocking_claims' => $blocking_claims]);

    // iterate over all blocking claims
    each($blocking_claims, function ($blocking_values, $blocking_key) use (&$blocked, $claims): void {
        //dbg(['blocking key' => $blocking_key, 'blocking values' => $blocking_values]);

        // check if the blocking key exists in the user claims
        if (array_key_exists($blocking_key, $claims)) {

            // extract blocked values, AND make them lowercase for comparison
            $blocked_values = map(explode(',', $blocking_values), fn($e) => mb_strtolower(trim($e)));
            //dbg(['blocked_values' => $blocked_values]);

            // compare against the lowercase'ed value of the user claims value
            if (contains($blocked_values, mb_strtolower($claims[$blocking_key]))) {
                //print_r(['blocked' => [$blocking_key, $claims[$blocking_key]]]);
                $blocked[$blocking_key] = $claims[$blocking_key];
            }
        }
    });

    return $blocked;
}

function check_mandatory_claims(array $claims): array
{
    // accumulating missing claims to be returned as result
    $missing = [];

    // enforce user claim keys to be lowercase
    $claims = reduce($claims, fn($v, $k, $c, $acc) => array_merge($acc, [mb_strtolower($k) => $v]), []);

    $mandatory_claims = parse_multi_line_claims_format(oidc('mandatory-claims', ''));
    // print_r(['mandatory-claims' => json_encode($mandatory_claims)]);
    each($mandatory_claims, function ($mandatory_values, $mandatory_key) use (&$missing, $claims): void {
        // print_r([$mandatory_key => $mandatory_values]);
        if (array_key_exists($mandatory_key, $claims)) {

            // extract mandatory values, AND make them lowercase for comparison
            $mandatory = map(explode(',', $mandatory_values), fn($e) => mb_strtolower(trim($e)));

            // compare against the lowercase'ed value of the user claims value
            if (!contains($mandatory, mb_strtolower($claims[$mandatory_key]))) {
                // print_r(['missing' => [$mandatory_key, $claims[$mandatory_key]]]);
                //$missing[] = [$mandatory_key, $claims[$mandatory_key]];
                $missing[$mandatory_key] = $mandatory_values;
            }
        } else {
            $missing[$mandatory_key] = $mandatory_values;
        }
    });

    return $missing;
}
