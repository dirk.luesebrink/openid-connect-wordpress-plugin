<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

// register the plugin settings admin menu
add_action("admin_menu", function (): void {
    add_submenu_page(
        "options-general.php",          // Which menu parent
        "Healy OpenID Connect",         // Page title
        "Healy OpenID Connect",         // Menu title
        "manage_options",               // Minimum capability (manage_options is an easy way to target Admins)
        "healy-oidc",                   // Menu slug
        fn() => render_settings_page()  // Callback that prints the markup
    );
});

// render html markup for the settings page
function render_settings_page(): void
{
    if (! current_user_can("manage_options")) {
        wp_die(__("You do not have sufficient permissions to access this page."));
    }

    if (isset($_GET['status']) && $_GET['status'] === 'success') {
        render_success_message();
    }

    //$settings = get_option('healy-openid-connect', []);
    render_settings_markup(); // $settings);
}

function render_success_message(): void { ?>
    <div id="message" class="updated notice is-dismissible">
        <p><?php _e("Healy OpenId Connect Settings updated!", "healy-openid-connect-plugin"); ?></p>
        <button type="button" class="notice-dismiss">
            <span class="screen-reader-text"><?php _e("Dismiss this notice.", "healy-openid-connect-plugin"); ?></span>
        </button>
    </div>
<?php }


function render_settings_markup(): void { ?>
<div style="width: 70%; padding: 0 3em 0 0">
    <form method="post" action="<?php echo admin_url('admin-post.php'); ?>">
        <?php settings_fields('healy-openid-connect'); ?>
        <input type="hidden" name="action" value="update_healy_oidc_settings" />
        <h3>Healy World OpenId Connect Log-in</h3>
        <?php render_metrics() ?>
        <?php
            render_form_input("Application (client) ID", "client_id");
            render_form_input("Authorization Endpoint", "authorization_endpoint");
            render_form_input("Scope", "scope", ['default' => 'openid']);
            render_form_input("Landing Page (on successful login/authentication)", "landing-page");
            render_form_input("Error Page", "error-page");
            // render_form_checkbox("Overwrite Login URL", "overwrite-login-url");
        ?>
        <h3>User Creation & Provisioning</h3>
        <p>You need to switch this on when you want that user accounts are
        automatically created. This is for users which have a valid account, so they
        can log-in on the identity server but do not yet have an account on
        this site.</p>
        <?php
            render_form_checkbox("Auto-Register New Users", "auto-register-new-users", [
                'title' => 'Only when checked new users will get an account created.'
            ]);
        ?>
        <h3>User Identification</h3>
        <p>This is about how to match the user from the incoming OpenId Connect
        Log-in with the userbase in this WordPress site. On WordPress there are
        multiple options to search for a user, this is the first option you have to
        select.</p>
        <?php
            render_form_radio_row(
                "WordPress User Identified by",
                "wp-user-identifying-field",
                ['ID', 'slug', 'email', 'login'],
                ['default' => 'email']
            );
        ?>
        <p>The second input here is about which attribute in the incoming login
        token does match to that WordPress user. The default here usually works
        and is to use the 'email' attribute from the incoming log-in and find the wordpres
        user by that 'email' value.</p>
        <?php
            render_form_input("Which claim in ID-Token identifies WordPress User? (defaults to same as WordPress field when left empty)", "claim-user-identifying-field");
        ?>

        <h3>User Meta & Claim handling</h3>
        <p>
            WordPress User Meta fields can be mapped from OpenId Connect
            claims. There is one mapping per line. Mappings are the WordPress user meta
            field is on the left and the OpenId Connect field (claim) is on the
            right. Only meta fields for which a mapping exists are written! All
            fields (claims) which are not mapped are NOT written to the
            WordPress user meta data.  Mappings can be defined differently for Log-in and
            account creation. That allows you to write some user meta field only at
            creation but not update them on later log-ins for example. Mappings lines
            are in the form:<br/><code>wordpress-meta-field: claim-field</code><br/>
        </p>
        <?php
            render_form_textarea(
                "User Meta Fields mapping on Account Creation (Auto-Register):",
                "update-on-create-field-mapping"
            );

            render_form_textarea(
                "User Meta Fields mapping on OpenID-Connect Log-in:",
                "update-on-login-field-mapping"
            );
        ?>

        <h3>Access Control by Claims</h3>
        <p>This settings guard login and provisioning
        of users entering by open-id connect. Any claim of the user which is
        listed in 'Blocking Claims' will block the user from getting into the
        system. 'Mandatory Claims' means that <i>all</i> listed claims must be
        present in the OIDC reques. With one of the listed values.
        Otherwise the user is also blocked from entry. Claims are listed
        here as list of key, values pairs.</p>

        <p>To exclude everybody not having a country claim of either
        australia or new zealand you could add this to <a>Mandatory Claims</a>:
        <code>country: au, nz</code>.</p>

        <p>To exclude everbody with a certain smell, you could add this
        line to <a>Blocking Claims</a>: <code>smell: oleander, lavender</code></p>
        <p>Please be aware that settings are case-sensitive.</p>
        <?php
            render_form_checkbox("Block Log-ins", "block-log-in");
            render_form_checkbox("Block automatic User creation & provisioning", "block-auto-register");
            render_form_textarea("Mandatory Claims", "mandatory-claims");
            render_form_textarea("Blocking Claims", "blocking-claims");
        ?>

        <h3>Roles from Claims and Filters</h3>

        <p>This plugin provides a filter
        (<code>healy-openid-connect-user-roles</code>) to add or removed WordPress
        roles from user accounts based on the openid-connect claims which
        are provided for the user. This for example can be used to automatically give
        admin rights to all users which have <code>@company.com</code>
        e-mail adress. The below two fields are to adjust that behaviour.
        </p>
        <?php
            render_form_input("Protected User Roles. (Roles from this list are protected from removal)", "protected-roles");
            render_form_input("Untouchable Users by Role. (Users in this list are completely excluded from programatically adding or removing roles)", "untouchable-roles");
        ?>

        <p>
            <input class="button button-primary" type="submit" value="<?php _e("Save", "healy-openid-connect-plugin"); ?>" />
        </p>
    </form>
</div>
<?php }

function render_metrics(): void { ?>
    <div style="padding: 0.5em 0">
        <p>Metrics: <?php render_plugin_metrics(); ?></p>
    </div>
<?php }

function render_plugin_metrics(): void {
    [$plugin_metrics, $_] = metrics()->values();
    $entries = [];
    foreach ($plugin_metrics as $k => $v) {
        $entries[] = "{$k}({$v})";
    }
    echo(join(', ', $entries));
}

function render_form_input(string $label, string $key, array $options = []): void { ?>
    <div style="padding: 0.5em 0">
        <label style="display: block" for="<?php echo $key ?>"><?php echo $label; ?></label>
        <input style="display: block"
            id="<?php echo $key ?>"
            name="healy-openid-connect[<?php echo $key ?>]"
            class="regular-text" type="text"
            <?php render_title($options['title'] ?? null) ?>
            value="<?php echo oidc($key, $options['default'] ?? null) ?>"
        />
    </div>
<?php }

function render_form_checkbox(string $label, string $key, array $options = []): void { ?>
    <?php $left_label = ('left' === ($options['label'] ?? null)) ?>
    <div style="padding: 0.5em 0">
        <?php if ($left_label) : ?>
            <label for="<?php echo $key ?>" <?php render_title($options['title'] ?? null) ?>>
                <?php echo $label; ?>
            </label>
        <?php endif;  ?>
        <input
            id="<?php echo $key ?>"
            name="healy-openid-connect[<?php echo $key ?>]"
            type="checkbox"
            class="regular-text" type="text"
            value="1"
            <?php checked(oidc($key), 1); ?>
        />
        <?php if (!$left_label) : ?>
            <label for="<?php echo $key ?>" <?php render_title($options['title'] ?? null) ?>>
                <?php echo $label; ?>
            </label>
        <?php endif;  ?>
        <?php render_helper_span($options['help'] ?? ''); ?>
    </div>
<?php }

function render_form_radio_row(string $label, string $key, array $values, array $options = []): void { ?>
    <div style="padding: 0.5em 0">
        <fieldset>
            <legend><?php echo $label ?>: </legend><?php
            foreach ($values as $value) {
                $checked = checked(oidc($key, $options['default'] ?? null), $value, false);
                echo <<<EOT
                    <label for="{$key}-{$value}">{$value}</label>
                    <input
                        id="{$key}-{$value}"
                        name="healy-openid-connect[{$key}]"
                        type="radio"
                        class="regular-text" type="text"
                        value="{$value}"
                        {$checked}
                    />
                EOT;
            }
            ?>
        </fieldset>
    </div>
<?php }

function render_title(string $title = null): void {
    if ($title) {
        echo "title='{$title}'";
    }
}

function render_helper_span(string $help = null): void { ?>
    <span class="helper"><?php echo $help ?></span>
<?php }

function render_form_textarea(string $label, string $key): void { ?>
    <div style="padding: 0.5em 0">
        <label style="display: block" for="<?php echo $key ?>"><?php echo $label; ?></label>
        <textarea style="display: block"
            id="<?php echo $key ?>"
            name="healy-openid-connect[<?php echo $key ?>]"
            rows="5" cols="50"
        ><?php echo oidc($key, '') ?></textarea>
    </div>
<?php }


// catches the form posted in 'render_settings_markup' call with the hidden 'action' input:
//
//  <input type="hidden" name="action" value="update_healy_oidc_settings" />
//
add_action('admin_post_update_healy_oidc_settings', function (): void {
    dbg("admin_post_update_healy_oidc_settings");

    // XXX better retrieve the _POST from a functions' argument instead of a global
    $fields = $_POST["healy-openid-connect"] ?? [];
    $fields = stripslashes_deep($fields);
    dbg([
        '_POST' => json_encode($_POST['healy-openid-connect']),
        'sanitized' => $fields
    ]);
    update_option("healy-openid-connect", $fields, true);

    // after successful safe, get back to the settings page
    $settings_page_url = "/wp-admin/options-general.php?page=healy-oidc&status=success";
    header("Location: " . $settings_page_url);
    exit;
});
