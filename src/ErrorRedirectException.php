<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

use Exception;
use Throwable;

class ErrorRedirectException extends Exception
{
    public string $url;

    public function __construct(string $url) {
        $this->url = $url;
        parent::__construct("error redirect: {$url}");
    }
}
