<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect;

use Firebase\JWT\JWK;
use Firebase\JWT\JWT;

use Healy\OpenIdConnect\Plugin\Error;

use function Healy\OpenIdConnect\Plugin\{ log, dbg };

// autocatch
function autocatch(callable $f): callable
{
    return function (...$args) use ($f): array {
        try {
            return [$f(...$args), null];
        } catch (\Throwable $ex) {
            return [null, $ex];
        }
    };
}

/**
 * returns the id_token payload as stdObject. If the id_token can not be
 * validated (signature, timed out, etc.), an excpetion is thrown.
 *
 * Assumuably this is the only method in this file you will ever use from the
 * outside.
 */
function validate_and_extract_id_token_payload(string $id_token): array
{
    list($jwks, $err) = autocatch(__NAMESPACE__ . '\load_id_token_jwks')($id_token);
    if ($err) {
        dbg([
            'validate_token error' => (string) $err,
            'callstack' => (new \Exception())->getTraceAsString()
        ]);
        return [null, new Error(Error::INVALID_JWT_TOKEN, ['token' => $id_token])];
    }
    dbg(['jwks loaded' => $jwks]);

    list($payload, $err) = validate_token($id_token, $jwks);
    if ($err !== null) {
        dbg([
            'validate_token error' => (string) $err,
            'callstack' => (new \Exception())->getTraceAsString()
        ]);
        return [null, $err];
    }
    dbg(['id_token payload validated' => $payload]);

    return [(object) $payload, null];
}

function load_id_token_jwks(string $token): array
{
    // load configuration and get JWKS url from it
    //$oidc_config = load_oidc_configuration($issuer);
    $oidc_config = load_oidc_configuration_for_token($token);
    $jwks_uri = $oidc_config->jwks_uri;

    // 3. load the jwks file and return as json
    $jwks = json_decode(file_get_contents($jwks_uri), true);

    // 4. finally, return it as firebase/php-jwt compatible key set objet
    return JWK::parseKeySet($jwks);
}

function load_end_session_endpoint(string $id_token): ?string
{
    $c = load_oidc_configuration_for_token($id_token);
    dbg(['load_oidc_configuration_for_token' => json_encode($c)]);
    return $c->end_session_endpoint ?? null;
}

function load_oidc_configuration_for_token(string $id_token): ?object
{
    list($parts, $err) = autocatch(__NAMESPACE__ . '\deconstruct_id_token')($id_token);
    if ($err) {
        return null;
    }

    // 1. unpack the token payload and get the issuer
    list(, $payload,) = $parts;
    //$issuer = id_token_issuer($token);
    $issuer = $payload->iss;

    // 2. load configuration and get JWKS url from it
    return load_oidc_configuration($issuer);
}

// returns the OIDC well known configuration for given issuer
function load_oidc_configuration(string $issuer): object
{
    dbg([__METHOD__ => $issuer, 'oidc config url' => $issuer . '/.well-known/openid-configuration']);

    static $cache = [];

    $config = $cache[$issuer] ?? null;
    if ($config === null) {
        //echo(__FUNCTION__ . ' loading: ' .  "{$issuer}/.well-known/openid-configuration\n");
        $config = file_get_contents($issuer . '/.well-known/openid-configuration');
        if (empty($config)) {
            throw new \Exception("config not found at: '{$issuer}/.well-known/openid-configuration'");
        }
        $config = json_decode($config);
    }
    $cache[$issuer] = $config;

    return $config;
}

function validate_token(string $token_s, array $jwks): array {
    try {
        $keys = $jwks ?? public_key();
        $payload = JWT::decode($token_s, $keys, array('RS256'));
        return [$payload, null];

    } catch (\Exception $e) {
        // return [null, [$e->getMessage() . ' -- ' . $token_s, $e]];
        return [null, new Error(Error::TOKEN_VALIDATION_FAILED, ['message' => $e->getMessage(), 'token' => $token_s])];
    }
}

function deconstruct_id_token(string $token): array {
    list($header, $payload, $signature) = explode('.', $token);
    $header = jwks_decode($header);
    $payload = jwks_decode($payload);

    return [$header, $payload, $signature];
}

function jwks_decode(string $in): object {
    return json_decode(base64_decode(str_replace('_', '/', str_replace('-', '+', $in))));
}

function public_key(?object $config = null): \OpenSSLAsymmetricKey {
    throw \Exception("not implemented: " . __METHOD__);
}
