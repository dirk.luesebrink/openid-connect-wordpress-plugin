<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

use function Functional\{each, map};

/*
 * there is something I really dislike on the wordpress do_action / add_action
 * functionallity here, but I can't change it:
 *
 * you can't type hint the add_action parameter because wordpress core fucks them up!
 *
 * Imagine you define a and action 'foo', and let it have one parameter, lets
 * say an options array $opts, which is an empty array on default, e.g:
 *
 *    funtion foo(array $opts = []): void
 *    {
 *         // do exactly nothing!
 *    }
 *
 * now register it with wordpress. Natural would be:
 *
 *      add_action('foo', 'foo', 10, 1);
 *      // or like this, which is handy if you want to have that function in local namespace:
 *      add_action('foo', fn(array $a = []) => foo($a), 10, 1);
 *
 * this kind of works, but it doesn't. If you call it someplace like:
 *
 *      do_action('foo', ['a' => 23])
 *
 * everything is fine. But when you call it like this: do_action('foo'), then suddenly your function is called as:
 *     foo("")
 *
 * WTF? The missing param on the do_action call is subsituted by the WordPress
 * core with an empty string... (auf so ne bescheuerte idee muss man erstmal
 * kommen).
 *
 * So, in consequence, none of this is possible:
 *      add_action('foo', fn(array $a = []) => foo($a), 10, 1);
 *      add_action('foo', fn(mixed $a = null) => foo($a), 10, 1);
 *
 * just keep koding like it's party over 1999....
 */

add_action('healy-oidc-login-link', static function ($params = null): void {
    $params ??= [];

    $prolog = $params['prolog'] ?? '';
    $epilog = $params['epilog'] ?? '';
    $text = $params['text'] ?? 'Healy-Sync Identity Platform Log-in';

    $clazz = $params['class'] ?? '';
    $url = render_authorization_endpoint_url();
    echo "{$prolog}<a class='{$clazz}' href='{$url}'>{$text}</a>{$epilog}";
}, 99, 1);

add_action('healy-oidc-login-button', static function ($params = null): void {
    $params ??= [];

    $prolog = $params['prolog'] ?? '';
    $epilog = $params['epilog'] ?? '';
    $text = $params['text'] ?? 'Healy-Sync Identity Platform Log-in';

    $clazz = $params['class'] ?? '';
    $url = render_authorization_endpoint_url();
    echo "{$prolog}<a href='{$url}'><button class='{$clazz}'>{$text}</button></a>{$epilog}";
}, 99, 1);

add_shortcode('healy-oidc-login', function ($atts = [], ?string $content = null, string $tag = ''): string {
    $url = render_authorization_endpoint_url();
    $content || ($content = "Healy-Sync Identity Platform Log-in");
    $link_class = $atts['link-class'] ?? '';
    return "<a class='{$link_class}' href='{$url}'>{$content}</a>";
});

add_action('healy-oidc-login-errors', static function ($options = null): void {
    $options ??= [];

    $errors = unpack_errors($_GET['e'] ?? []);
    if (count($errors) === 0) {
        return;
    }

    $clazz = $options['class'] ?? 'healy-oidc-login-errors';
    echo "<div id='{$clazz}'><ul>" . render_errors($errors) . '</ul></div>';
}, 99, 1);

add_shortcode('healy-oidc-errors', static function ($options = null): string {
    $options ??= [];

    $errors = unpack_errors($_GET['e'] ?? []);
    if (count($errors) === 0) {
        return "";
    }

    $clazz = $options['class'] ?? 'healy-oidc-login-errors';
    return "<ul id='{$clazz}'>" . render_errors($errors, $options) . "</ul>";
}, 99, 3);

function unpack_errors(array $errors): array
{
    return map($errors, fn ($e) => unpack_error(stripslashes_deep(urldecode($e))));
}

function unpack_error(string $error_json): Error
{
    list($code, $params) = json_decode($error_json, true);
    return new Error($code, $params);
}

function render_errors(array $errors, array $options = []): string
{
    $clazz = $options['error-class'] ?? 'healy-oidc-login-error';

    //return join("\n", map($errors, fn ($error) => render_reason($error)));
    return join("\n", map(
        map($errors, fn ($error) => render_reason($error)),
        fn ($reason) => "<li id='{$clazz}'>{$reason}</li>"
    ));
}

function render_reason(Error $error): string
{
    return (string) $error;
}
