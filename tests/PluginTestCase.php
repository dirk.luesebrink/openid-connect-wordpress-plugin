<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin\Test;

use PHPUnit\Framework\TestCase;

require __DIR__ . '/../vendor/autoload.php';

// include plugin functionallity which we want to test, the ones which are not
// to contaminated with wordpress coupling
require_once __DIR__ . '/../src/functions.php';

// overload global wordpress functions in the plugin namespace
require_once __DIR__ . '/wordpress-mocking.php';

abstract class PluginTestCase extends TestCase
{
}
