<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin\Test;

use Healy\OpenIdConnect\Plugin\ErrorRedirectException;
use Healy\OpenIdConnect\Plugin\Test\PluginTestCase;

final class ErrorRedirectExceptionTest extends PluginTestCase
{
    public function test_has_public_url_property(): void
    {
        $this->assertEquals('foo', (new ErrorRedirectException('foo'))->url);
    }
}
