<?php

declare(strict_types=1);

// mocking/hiding wordspace functions used by the Cookies class
namespace Healy\OpenIdConnect\Plugin;

define('COOKIE_DOMAIN', '.test.localhost');

function is_ssl(): bool
{
    return false;
}

function setcookie(
    string $name,
    $value = "",
    $expires_or_options = 0,
    $path = "",
    $domain = "",
    $secure = false,
    $httponly = false
): bool {
    return true;
}

function sanitize_text_field(string $in): string
{
    return $in;
}
// .end of wordpress mocking/hiding

namespace Healy\OpenIdConnect\Plugin\Test;

use Healy\OpenIdConnect\Plugin\Cookies;
use Healy\OpenIdConnect\Plugin\Test\PluginTestCase;

final class CookiesTest extends PluginTestCase
{
    public function test_namespace_lookup(): void
    {
         $this->assertTrue(class_exists('Healy\OpenIdConnect\Plugin\Cookies'));
    }

    public function test_set_get_clear_get(): void
    {
         $this->assertNull(Cookies::get('foo'));
         $this->assertTrue(Cookies::set('foo', 'joa'));
         $this->assertEquals('joa', Cookies::get('foo'));
         Cookies::clear('foo');
         $this->assertNull(Cookies::get('foo'));
    }
}
