<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin\Test;

use Healy\OpenIdConnect\Plugin\Error;
use Healy\OpenIdConnect\Plugin\Metrics;
use Healy\OpenIdConnect\Plugin\Test\PluginTestCase;

use function Healy\OpenIdConnect\Plugin\scratch;
use function Healy\OpenIdConnect\Plugin\scratch_op;
use function Healy\OpenIdConnect\Plugin\scratch_get;
use function Healy\OpenIdConnect\Plugin\scratch_set;
use function Healy\OpenIdConnect\Plugin\scratch_clear;
use function Healy\OpenIdConnect\Plugin\get_option;

final class MetricsTest extends PluginTestCase
{
    public function setUp(): void
    {
        scratch_clear();
    }

    public function test_has_a_public_constructor(): void
    {
        $this->assertInstanceOf(Metrics::class, new Metrics());
    }

    public function test_values_return_plugin_and_user_counter_tuple(): void
    {
        $m = new Metrics();
        $this->assertEquals([[], []], $m->values());
    }

    public function test_inc_inits_with_one(): void
    {
        $m = new Metrics();
        $m->inc('A');
        $this->assertEquals([['A' => 1], []], $m->values());
    }

    public function test_inc_per_plugin_counter(): void
    {
        $m = new Metrics();
        $m->inc('A');
        $this->assertEquals([['A' => 1], []], $m->values());
    }

    public function test_inc_error(): void
    {
        $m = new Metrics();
        $error = new Error(Error::CRSF_STATE_CHECK_FAILED);
        $m->incError($error);
        $this->assertEquals([["E{$error->code()}" => 1], []], $m->values());
    }

    public function test_inc_per_plugin_and_per_user_counter(): void
    {
        $m = new Metrics();
        $m->inc('A', 1234);
        $this->assertEquals([['A' => 1], ['A' => 1]], $m->values());

        // with a different user id updates different user counter but same plugin counter
        $m->inc('A', 4321);
        $this->assertEquals([['A' => 2], ['A' => 1]], $m->values());
    }

    public function test_inc_error_per_plugin_and_per_user(): void
    {
        $m = new Metrics();
        $error = new Error(Error::CRSF_STATE_CHECK_FAILED);
        $m->incError($error, 100);
        $this->assertEquals([["E{$error->code()}" => 1], ["E{$error->code()}" => 1]], $m->values());

        $m->incError($error, 101);
        $this->assertEquals([["E{$error->code()}" => 2], ["E{$error->code()}" => 1]], $m->values());
    }

    public function test_save(): void
    {
        $m1 = new Metrics();

        $m1->inc('a');
        $m1->inc('a', 1234);
        $m1->inc('c', 1234);
        $this->assertEquals([['a' => 2, 'c' => 1], ['a' => 1, 'c' => 1]], $m1->values());

        $m1->save();
        $this->assertEquals(['a' => 2, 'c' => 1], \Healy\OpenIdConnect\Plugin\get_option(Metrics::PLUGIN_KEY));
        $this->assertEquals(['a' => 1, 'c' => 1], \Healy\OpenIdConnect\Plugin\get_user_meta(1234, Metrics::USER_KEY));
    }

    public function test_save_refresh(): void
    {
        $m1 = new Metrics();

        $m1->inc('a');
        $m1->inc('a', 1234);
        $m1->inc('c', 1234);
        $this->assertEquals([['a' => 2, 'c' => 1], ['a' => 1, 'c' => 1]], $m1->values());

        // m2 is not 'seeing' m1 values until m1 has been saved and m2 is refreshed
        $m2 = new Metrics();
        $this->assertEquals([[],[]], $m2->values());
        // m2 refresh leaves m1 still invisible to m2
        $m2->refresh();
        $this->assertEquals([[],[]], $m2->values());

        // saving m1 is not enough to make it into m2
        $m1->save();
        $this->assertEquals([[],[]], $m2->values());

        // only m2 refresh, WITH THE SAME USER ID!, leads to seeing m1 saved values
        $m2->refresh(1234);
        //echo('m1: ' . json_encode($m1->values()) . "\n");
        //echo('m2: ' . json_encode($m2->values()) . "\n");
        $this->assertEquals($m1->values(), $m2->values());
    }

    public function test_save_loop(): void
    {
        foreach (range(1, 10) as $i) {
            $m = new Metrics();
            $m->inc('a', 1234);
            //echo('1 m: ' . json_encode($m->values()) . "\n");
            $this->assertEquals([['a' => $i], ['a' => $i]], $m->values());
            $m->save();

            //$m = new Metrics();
            //echo('3 m: ' . json_encode($m->values()) . "\n");
            //$this->assertEquals([['a' => $i], ['a' => $i]], $m->values());
        }
    }

    public function test_inc_dec(): void
    {
        $m = new Metrics();
        $m->inc('A');
        $this->assertEquals([['A' => 1], [        ]], $m->values());
        $m->inc('A', 4321);
        $this->assertEquals([['A' => 2], ['A' => 1]], $m->values());
        $m->dec('A');
        $this->assertEquals([['A' => 1], ['A' => 1]], $m->values());
        $m->dec('A', 4321);
        $this->assertEquals([['A' => 0], ['A' => 0]], $m->values());
    }
}
