<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect;

// This is overloading (hiding) the global file_get_contents function in the
// namespace of the to be tested functions. this is to avoid the unit test
// hitting the network to read the key configuration of the server
function file_get_contents(string $uri): string
{
    switch ($uri) {
        case 'https://some.void/sso/.well-known/openid-configuration':
        // for below case see also comment in: function test_end_session_endpoint_from_token()
        case 'https://login.dev.healy.world/sso/.well-known/openid-configuration':
            static $count = 0;
            if (1 < $count) {
                throw new \Exception('oidc config loading must be cached!');
            }
            //echo("mocked file loading: {$uri}\n");
            $count += 1;
            return \file_get_contents(__DIR__ . '/well-known-openid-configuration.json');
        default:
            throw new \Exception("unexpected mocked file_get_contents call: {$uri}");
    }
}
// .end of mock overload

namespace Healy\OpenIdConnect\Plugin\Test;

use Healy\OpenIdConnect\Plugin\Error;

use function Healy\OpenIdConnect\load_end_session_endpoint;
use function Healy\OpenIdConnect\load_oidc_configuration;
use function Healy\OpenIdConnect\load_oidc_configuration_for_token;
use function Healy\OpenIdConnect\validate_and_extract_id_token_payload;

final class HealyOpenIdConnectTest extends PluginTestCase
{
    // function validate_and_extract_id_token_payload(string $id_token = null): array {
    public function test_validate_and_extract_id_token_payload_on_broken_token(): void
    {
        $this->assertIsArray([$payload, $err] = validate_and_extract_id_token_payload('ojtogjt'));
        $this->assertNull($payload);
        $this->assertInstanceOf(Error::class, $err);
    }

    public function test_build_well_known_configuration_path_from_issuer(): void
    {
        $issuer = 'https://some.void/sso';
        $this->assertIsObject($c = load_oidc_configuration($issuer));
    }

    public function test_load_oidc_configuration_is_cached(): void
    {
        $issuer = 'https://some.void/sso';
        $this->assertIsObject($c = load_oidc_configuration($issuer));
        $this->assertIsObject($c = load_oidc_configuration($issuer));
    }

    public function test_end_session_endpoint_from_oidc_configuration(): void
    {
        $issuer = 'https://some.void/sso';
        $this->assertIsString(
            'http://login.dev.healy.world/sso/end-session',
            load_oidc_configuration($issuer)->end_session_endpoint
        );
    }

    public function test_end_session_endpoint_from_token(): void
    {
        // this id token encodes 'https://login.dev.healy.world/sso' as issuer,
        // which is not so easy to fake because of the cryptographic signature.
        // So instead of replicating the whole signature flow, for the test I
        // copied an existing ID Token and use it here with a mocked server
        // response, see function file_get_contents(string $uri): above
        //
        $id_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImxvY2FsaG9zdC0yMDIyLTAxLTE2VDEyOjE3OjI1WiJ9.eyJwYXNzd29yZF9oYXNoIjoiJDJ5JDEwJG5jVlpyQTcybXVUaktFUUZDU2pwRWVqZ3c5VVcxcHFrM0RRbHJvWVZXY1VMUGFrbkZYb0ttIiwidXNlcm5hbWUiOiJjcnV4IiwiZW1haWwiOiJkaXJrLmx1ZXNlYnJpbmtAZ21haWwuY29tIiwiaGVhbHlfaWQiOiI1MTg5LTczMTUtNDQzNiIsInJvbGVzIjpbXSwic3BvbnNvciI6IjQxMDQtMjQxMC05MTMxIiwiYnVzaW5lc3Nfcm9sZSI6IkN1c3RvbWVyIiwiZmFtaWx5X25hbWUiOiJsXHUwMGZjc2VicmluayIsImdpdmVuX25hbWUiOiJkaXJrIiwiY291bnRyeSI6IkRFIiwicGhvbmUiOiIxMTIyMzM0NCIsImdlbmRlciI6Ik1hbGUiLCJzYWx1dGF0aW9uIjoiTXIuIiwiY29tcGFueV9uYW1lIjoiTXIuIENoYW5jZSBQcm9kdWN0aW9ucyBHbWJIIiwiYWRkcmVzcyI6eyJzdHJlZXRfYWRkcmVzcyI6IkxpbmUxIiwibG9jYWxpdHkiOiJiZXJsaW4iLCJyZWdpb24iOiIiLCJwb3N0YWxfY29kZSI6IjEwNDM3IiwiY291bnRyeSI6IkRFIn0sInNpZCI6IjRpZ2FCU3p6Y0xWMGJ5ZmVWdmI0T01SOER4M1JoYXpqajI4c2hIUW4iLCJpc3MiOiJodHRwczovL2xvZ2luLmRldi5oZWFseS53b3JsZC9zc28iLCJpYXQiOjE2NjQxMzA1MzAsImV4cCI6MTY2NDEzNDEzMH0.xaqg5VrApI8-bhqkhZ-Wu6G-Is_vwD90MFJtfAYh6orhDKjBbtRIlXXZ0YxtHbBy75yrwUiRESdIGYXH_dqIvKXxMaPkZs_RAs4BgS9lSvo5J6tMyf6tjAPxAQYbeEitJIRXpI8cj-A62BDddKlI5rmWlkxCwXuHt50UPjoBIUClyQHlBIYL2fdpy9XQoDEyhKiN2B52e2vEHR08j2Zhke2J6fVaFP343cLlV9hhgr2nCJBBzFJoVFN-iKwZD7w-pPjZkneVc8PtJS3j4uOYAB1C8mHcUHMahH9nP_0XkPHufeFXyNd6Ng_zKSPR5K09BKetjh_KIcikSYE3cBYN1g';
        $this->assertIsString('http://login.dev.healy.world/sso/end-session', load_end_session_endpoint($id_token));
    }

    public function test_load_oidc_configuration_for_token(): void
    {
        $id_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImxvY2FsaG9zdC0yMDIyLTAxLTE2VDEyOjE3OjI1WiJ9.eyJwYXNzd29yZF9oYXNoIjoiJDJ5JDEwJG5jVlpyQTcybXVUaktFUUZDU2pwRWVqZ3c5VVcxcHFrM0RRbHJvWVZXY1VMUGFrbkZYb0ttIiwidXNlcm5hbWUiOiJjcnV4IiwiZW1haWwiOiJkaXJrLmx1ZXNlYnJpbmtAZ21haWwuY29tIiwiaGVhbHlfaWQiOiI1MTg5LTczMTUtNDQzNiIsInJvbGVzIjpbXSwic3BvbnNvciI6IjQxMDQtMjQxMC05MTMxIiwiYnVzaW5lc3Nfcm9sZSI6IkN1c3RvbWVyIiwiZmFtaWx5X25hbWUiOiJsXHUwMGZjc2VicmluayIsImdpdmVuX25hbWUiOiJkaXJrIiwiY291bnRyeSI6IkRFIiwicGhvbmUiOiIxMTIyMzM0NCIsImdlbmRlciI6Ik1hbGUiLCJzYWx1dGF0aW9uIjoiTXIuIiwiY29tcGFueV9uYW1lIjoiTXIuIENoYW5jZSBQcm9kdWN0aW9ucyBHbWJIIiwiYWRkcmVzcyI6eyJzdHJlZXRfYWRkcmVzcyI6IkxpbmUxIiwibG9jYWxpdHkiOiJiZXJsaW4iLCJyZWdpb24iOiIiLCJwb3N0YWxfY29kZSI6IjEwNDM3IiwiY291bnRyeSI6IkRFIn0sInNpZCI6IjRpZ2FCU3p6Y0xWMGJ5ZmVWdmI0T01SOER4M1JoYXpqajI4c2hIUW4iLCJpc3MiOiJodHRwczovL2xvZ2luLmRldi5oZWFseS53b3JsZC9zc28iLCJpYXQiOjE2NjQxMzA1MzAsImV4cCI6MTY2NDEzNDEzMH0.xaqg5VrApI8-bhqkhZ-Wu6G-Is_vwD90MFJtfAYh6orhDKjBbtRIlXXZ0YxtHbBy75yrwUiRESdIGYXH_dqIvKXxMaPkZs_RAs4BgS9lSvo5J6tMyf6tjAPxAQYbeEitJIRXpI8cj-A62BDddKlI5rmWlkxCwXuHt50UPjoBIUClyQHlBIYL2fdpy9XQoDEyhKiN2B52e2vEHR08j2Zhke2J6fVaFP343cLlV9hhgr2nCJBBzFJoVFN-iKwZD7w-pPjZkneVc8PtJS3j4uOYAB1C8mHcUHMahH9nP_0XkPHufeFXyNd6Ng_zKSPR5K09BKetjh_KIcikSYE3cBYN1g';
        $this->assertIsObject($o = load_oidc_configuration_for_token($id_token));
        //echo('oidc config: ' . json_encode($o));
    }
}
