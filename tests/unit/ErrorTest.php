<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin\Test;

use Healy\OpenIdConnect\Plugin\Error;
use Healy\OpenIdConnect\Plugin\Test\PluginTestCase;

final class ErrorTest extends PluginTestCase
{
    public function test_blocked_by_claims(): void
    {
        $this->assertEquals(
            '[E1] Login blocked by profile fields: {"foo":"bar","xxx":23}',
            (string) new Error(
                Error::BLOCKED_BY_CLAIMS,
                ['claims' => ['foo' => 'bar', 'xxx' => 23]],
            )
        );
    }

    public function test_error_params(): void
    {
        $error = new Error(Error::MISSING_CLAIMS, ['claims' => ['some' => 'claim']]);
        $this->assertEquals(['claims' => ['some' => 'claim']], $error->params());
    }

    public function test_error_to_string(): void
    {
        $this->assertEquals(
            '[E2] Blocked because of missing mandatory profile fields: {"some":"claim"}',
            (string) new Error(Error::MISSING_CLAIMS, ['claims' => ['some' => 'claim']])
        );
    }

    public function test_text(): void
    {
        $this->markTestSkipped('text() method set private, might get removed');
        $this->assertEquals(
            '[E2] Blocked because of missing mandatory profile fields: {{claims}}',
            (new Error(Error::MISSING_CLAIMS, ['claims' => ['some' => 'claim']]))->text()
        );
    }
}
