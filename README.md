# Healy World OpenId Connect WordPress Plugin

Use the OpenId Connect Protocol with your Account at an Identity Service Provider to log in and/or register new users on your WordPress site.

For integration of web sites, services and mobile applications in the healy digital eco system Healy World provides a centralized Identity Provider. Authentification of any account can be provided from there without leaking passwords or account secrets to the participating services or apps without proper authorization of the the user itself or without authorization of the Healy organization.

## Standards

The desired features (cross site and application secure authentification and profile sharing) depend on a functioning inter-operation of multiple systems under different authority. It becomes immediately obvious that this must be build on exactly defined open standards which are accessible to everybody independently from any one participating party. In case of identity management, the standards we are building upon are OAuth2 and OpenID Connect. Please be aware we are using OpenID Connect and NOT OpenID. OpenID is an obsolete and not longer supported by the OpenID Foundation.

- [Specifications | OpenID](https://openid.net/developers/specs/)
- [What is OpenID Connect?](https://www.scottbrady91.com/openid-connect/openid-connect-overview)

## OpenID Connect with Healy Sync

OpenID Connect (OIDC) provides a simple identity layer on top of the OAuth 2.0 protocol, enabling Single Sign-On (SSO) and API access in one round trip. OIDC is integrated with, and provided by, the Healy-Sync application server.

# OpenId Connect Healy-Login  WordPress Client

## Plugin Settings

To use and connect with any OpenId Connect Identity Provider you need a set parameters which are defined in the plugin settings. 

<img title="" src="images/Plugin-Settings.png" alt="WordPress Settings" data-align="center">

After successful authentification of the user, your app, as a client of the Healy World OpenID Connect will receive an JWT `id_token` which contains the claims for the authentificated user. 

### Scopes & Clients

Healy-Login and this WordPress plugin implement the following scopes:

* openid
* profile
* business

This are the available claims, according to the scopes: 

* openid
  * username
  * email
* profile claims:
  * healy_id
* business claims:
  * family_name    
  * given_name     
  * sponsor 
  * business_status (Member, Custumer,...)
  * country
  * address

## Metrics 

While the plugin is active, it will collect some usage numbers. Theses number are persisted as meta plugin data in the wordpress db and are also shown on top of the plugin admin settings page like this: 

<img title="" src="images/Plugin-Metrics.png" alt="Plugin Metrics" data-align="center">

These Metrics counter and their meanings are as follows:

**total-logons-requests(246)**: Number of all (via REST API) incoming
OpenId-Connect Login request.

**identified-request(161)**: Every request contains parameters, the JWT ID
Token, which needs to be decoded.  When all neccesary parameters are present
and the ID token can be correctly decoded, once the username for the request is
found, it will be counted as `identified`.

**unique-users(92)**: For every successful login, it is marked for the user if
he used the OpenId Connect login before. Every unique user ever used this
log-in is counted as 1 in this counter.

**successful-logins(161)**: As stated, the number of successful log-in in
total. Users are counted qs often as they log in.

**completed-logon-requests(161)**: `todo: what is the difference from
**completed** to **succeesful**?`

**failed-requests(72)**: This can be multiple different things. The counter
itself does only report how many times the user was redirected to an error page
because somethings were wrong with the request. This includes: blocking by
claims, token validation, auto-register settings or CSRF state check.

**auto-registered-users(14)**: When the settings allow it, validated user
log-ins, which are not yet known on the receiving site, will trigger a
registration of a new wordpress user account, local to this site. 

**erroneous-requests(13)**: This is the number of catch-all unhandled errors.
This number is included in the number of failed requests.


