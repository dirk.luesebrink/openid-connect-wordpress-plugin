<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

/*
   Plugin Name: Healy World OpenID Connect
   Plugin URI: https://healy.world/
   description: Log in with your Healy World Account
   Version: 0.1.5
   Author: Dirk Lüsebrink
   Author URI: https://healy.world
   License: MIT
*/
defined('ABSPATH') or die('script kiddies go home!');

// autoloading for composer dependencies. Check for file existance first, could
// be we are running inside a wordpress composer install environment in which
// case the autoload file is elsewhere
if (is_readable(__DIR__ . '/vendor/autoload.php')) {
    require __DIR__ . '/vendor/autoload.php';
}

use Healy\OpenIdConnect\Plugin\Cookies;
use Healy\OpenIdConnect\Plugin\ErrorRedirectException;
use Healy\OpenIdConnect\Plugin\Metrics;

// containing code to handle the JWKS/OpenID server responses
require_once __DIR__ . '/src/healy-openid-connect.php';
use function Healy\OpenIdConnect\validate_and_extract_id_token_payload;
use function Healy\OpenIdConnect\load_end_session_endpoint;

// shortcodes
require_once __DIR__ . '/src/demo-login-page-shortcode.php';
require_once __DIR__ . '/src/login-status-shortcode.php';
require_once __DIR__ . '/src/login-elements.php';

// functions separated from their registraton for better wordpress free
// testability
require_once __DIR__ . '/src/functions.php';

// the plugin settings in the admin area
require_once __DIR__ . '/src/admin-page.php';

const CSRF_TOKEN_COOKIE = "oidc-csrf-token";

add_action('init', function (): void {
    // ini_set('error_log', 'debug.log');
    // if (! session_id()) {
    //     session_start();
    // }

    if (!Cookies::get(CSRF_TOKEN_COOKIE)) {
        Cookies::set(CSRF_TOKEN_COOKIE, md5('' . rand()), time() + 2/*minutes*/ * 60 /*sekunden*/);
    }
});

function csrf_token(): ?string
{
    return Cookies::get(CSRF_TOKEN_COOKIE);
}

// define a shortcut to insert a URL anchor link which leads to authorization
// endpoint. Endpoint is part of the settings. Use like this anywhere in your
// page:
//
//    [healy-oidc-login]Healy Sync Login[/healy-oidc-login].
//
add_shortcode('healy-oidc-login', function ($atts = [], ?string $content = null, string $tag = ''): string {
    $url = render_authorization_endpoint_url();
    $content || ($content = "Healy-Sync Identity Platform Log-in");
    $link_class = $atts['link-class'] ?? '';
    return "<a class='{$link_class}' href='{$url}'>{$content}</a>";
});

add_filter('login_url', function (string $login_url, string $redirect, string $force_reauth): string {
    if (empty(oidc('overwrite-login-url'))) {
        return $login_url;
    }

    //dbg('filter(login_url): ' . json_encode(func_get_args()));
    $login_url = render_authorization_endpoint_url();
    //dbg('authorization endpoint url: ' . $login_url);

    if (! empty($redirect)) {
        $login_url = add_query_arg('redirect_to', urlencode($redirect), $login_url);
    }
    if ($force_reauth) {
        $login_url = add_query_arg('reauth', '1', $login_url);
    }

    //dbg('overwritten login url: ' . $login_url);
    return $login_url;
}, 10, 3);

add_filter('logout_redirect', function (string $redirect_to, string $requested_redirect_to, \WP_User $user): string {
    $oidc_landing_page = oidc('landing-page'); //get_option('healy-sync-landing-page');
    if (!empty($oidc_landing_page)) {
        $redirect_to = $oidc_landing_page;
    }
    return $redirect_to;
}, 10, 3);

// wp_logout action is installed to forward logouts to the openid-connect
// server. This forward will only happen when the server config contains an
// endsession-endpoint. Otherwise this action does nothing and returns
// immediately. The logout forwarding is non-blocking and asynchronous. It will
// not block the wordpress, even when the server it not reachable.
//
add_action('wp_logout', function (int $uid): void {
    // need id_token here and the end_session_endpoint
    // wp_remote_get( $url, array(blocking=>false) );
    dbg("oidc log-out user id: {$uid}");

    $meta = get_user_meta($uid, 'healy-openid-connect-logout-meta', true);
    if (!is_array($meta)) {
        // no valid logout info found, so just return
        return;
    }
    dbg('oidc log-out meta: ' . json_encode($meta));

    ['id_token_hint' => $id_token_hint, 'end_session_endpoint' => $end_session_endpoint] = $meta;
    $url = add_query_arg(['id_token_hint' => $id_token_hint], $end_session_endpoint);
    dbg("oidc log-out URL: {$url}");
    $rc = wp_remote_get($url, ['blocking' => false]);
});

function render_authorization_endpoint_url(): string
{
    $auth_endpoint = oidc('authorization_endpoint'); // get_option('healy-sync-authorization_endpoint');
    $client_id = oidc('client_id'); // get_option('healy-sync-client_id');
    $redirect_uri = get_rest_url(null, 'healy-sso/v1/logon');
    $state = csrf_token(); // md5(session_id()); // oidc_cookie()->val;
    // dbg([
    //     "auth_endpoint" => $auth_endpoint,
    //     "client_id"     => $client_id,
    //     "redirect_uri"  => $redirect_uri,
    //     "url"           => $url,
    // ]);
    return $auth_endpoint
        . "?client_id={$client_id}"
        . "&redirect_uri={$redirect_uri}"
        . "&scope=openid"
        . "&response_type=id_token"
        . "&response_mode=form_post"
        . "&state={$state}" // XXX check on receiving side to block CSRF? attacks
        . "&nonce=" . md5(csrf_token()) // {$state}"
        ;
}

// run on plugin activation
register_activation_hook(__FILE__, static function (): void {
    error_log(print_r('Healy OpendID Connect Plugin activated!', true));
});

// run on plugin de-activation
register_deactivation_hook(__FILE__, static function (): void {
    error_log(print_r('Healy OpendID Connect Plugin de-activated!', true));
});

add_action('rest_api_init', static function (): void {
    register_rest_route('healy-sso/v1', '/logon(\?(?P<params>.*))?', [
        'methods' => 'POST',
        'callback' => fn(\WP_REST_Request $request) => rest_api_logon($request),
        'permission_callback' => '__return_true',
    ]);
});

// // returns matching user metrics or creates a new one if the current one doesnt
// // match the id. When neither a id is provided nor a previously existing metric
// // can be returned and exceptions is thrown
// //
// function user_metrics(int $wp_user_id = -1): UserMetrics
// {
//     static $metrics = null;
//
//     if (0 < $wp_user_id) {
//         if ($metrics === null || ($metrics->wp_user_id() !== $wp_user_id)) {
//             $metrics = UserMetrics::load($wp_user_id);
//         }
//     } elseif ($metrics === null) {
//         throw new Exception(__METHOD__ . ' missing $wp_user_id to create metric instance');
//     }
//
//     return $metrics;
// }
function metrics(bool $refresh = false): Metrics
{
    static $metrics = null;

    if ($refresh || ($metrics === null)) {
        $metrics = new Metrics();
    }

    return $metrics;
}

// this is the function on the receiving side of the rest api route
// register_rest_route( 'healy-sso/v1', ...
//
// on success it returns a redirect to the landing page
//
// on error it returns a redirect to the error page
//
// landing page and error page are defined on the plugin admin settings page
//
function rest_api_logon(\WP_REST_Request $request): void
{
    // start a new metrics object on every request
    $m = metrics(true);

    // default redirect URL at ent of this function
    $redirect_url = landing_page_url();

    try {
        $m->inc(Metrics::TOTAL_LOGONS_REQUESTS);
        rest_api_logon_unsafe($request);
        $m->inc(Metrics::COMPLETED_LOGON_REQUESTS);

    } catch (ErrorRedirectException $ex) {
        $m->inc(Metrics::FAILED_REQUESTS);
        $redirect_url = $ex->url;

    } catch (\Throwable $ex) {
        $m->inc(Metrics::ERRONEOUS_REQUESTS);
        $redirect_url = error_page_redirection_url([new Error(
            Error::REST_API_LOGON_EXCEPTION,
            ['ex' => get_class($ex), 'msg' => '(internal) ' . $ex->getMessage()]
        )]);
        // todo: log callstack here, as this is an expected error condition which needs debugging

    } finally {
        // save gathered metrics on this request, no matter what
        $m->save();
    }

    // redirect to landing page, we're done here
    header("Location: {$redirect_url}");
    die;
}

// TODO: this function implements the complete login & user registration flow.
// It is much to long and need to be broken up in pieces/phases, but there are
// so many variation on what can happen here so I haven't yet found a good way
// to break it up.
//
function rest_api_logon_unsafe(\WP_REST_Request $request): void
{
    $body_params = $request->get_body_params();
    //dbg(["logon server body params" => $body_params]); // , "query params" => $request->get_query_params(),
    dbg('oidc logon params: ' . json_encode($body_params)); // , "query params" => $request->get_query_params(),

    // first, before even unpacking the id_token, let's check out CSRF state cookie protections
    if (!do_oidc_state_cookie_match($body_params['state'] ?? '')) {
        dbg('oidc logon CRSF state check failed');
        //return ['error' => 'oidc logon CRSF state check failed'];
        return_error_page_redirection([new Error(Error::CRSF_STATE_CHECK_FAILED)]);
    }
    // call action immediately after we checked for CSRF attack
    do_action('healy-openid-connect-login-request', $request);

    // unpack the OIDC token payload, containing info for the login
    $id_token = $body_params['id_token'] ?? null;
    list($claims, $error) = validate_and_extract_id_token_payload($id_token);
    // dbg('oidc claims: ' . json_encode($claims));
    if ($error) {
        return_error_page_redirection([$error]);
    }

    // access token allows for custom implementations in filters & actions
    $access_token = $body_params['access_token'] ?? null;

    // call into a filter gives the subscriber a chance to read/modify the
    // login claims.
    $claims = apply_filters('healy-openid-connect-claims-filter', $claims, $access_token);

    // Search database for existing WP_User
    $user = get_user_by_identifying_claim($claims);

    // blocked by claim?
    //list($blocked_by_claims, $blocking_reasons) = is_user_blocked((array)$claims);
    $blocking_reasons = blocking_reasons((array) $claims);
    $blocked_by_claims = (0 < count($blocking_reasons));

    // if the user does not exist in the database..
    if ($user === null) {
        // first check if auto-registering is maybe blocked by claims
        if ($blocked_by_claims && oidc('block-auto-register')) {
            //return ['error' => $blocking_reasons];
            return_error_page_redirection($blocking_reasons);
        }

        // ..give clients a chance to create one
        $user = apply_filters('healy-openid-connect-provision-new-user', null, $claims, $access_token);

        // if filter created no user and the auto-register is not enabled, we have an error exit here.
        if ($user === null) {
            if (!oidc('auto-register-new-users')) {
                //dbg('no wordpress user found, none created and auto-registering new users is off');
                //return ['error' => 'no wordpress user found, none created and auto-registering new users is off'];
                return_error_page_redirection([new Error(Error::AUTO_REGISTER_NOT_ENABLED)]);
            }
        }
    }

    // We do not know if the user was found or just created, but in any case we
    // update the user object with the latest attribute setting from the claims
    $user_params = provision_user_update_parameters($user, $claims, $access_token);

    // In case we already have a WP_User object here, we set its 'ID' in the
    // $user_params for the following wp_insert_call which makes it effectively
    // updating the existing user with the given parameters.
    if ($user) {
        // LOG-IN
        // existing user -> set ID on params to update it. we actually have
        // log-in attempt here
        $user_params['ID'] = $user->ID;
        $claims_mapping = mapped_claims("update-on-login-field-mapping");
    } else {
        // CREATE
        // creating new user -> set random password
        $user_params['user_pass'] = wp_generate_password(10);
        $claims_mapping = mapped_claims("update-on-create-field-mapping");
        metrics()->inc(Metrics::AUTO_REGISTERED_USERS);
    }

    // If no ID param is set the user will be created at this point.
    $rc = wp_insert_user($user_params);
    if (!is_int($rc)) {
        $error_message = $rc->get_error_message($rc->get_error_code());
        //dbg($error_message);
        // return ['error' => $error_message];
        return_error_page_redirection([new Error(
            Error::WORDPRESS_INSERT_USER_ERROR,
            ['msg' => $rc->get_error_message($rc->get_error_code())]
        )]);
    }
    // The non integer return code type from the wordpress insert function
    // indicates we have an actuall wordpress user object here.
    $user = new \WP_User($rc);

    // habemus WP user! At this point we have a wordpress user object! Either
    // found or created from filter

    // log-in can be blocked by claims, for existing but as well for newly created users
    if ($blocked_by_claims && oidc('block-log-in')) {
        metrics()->inc(Metrics::BLOCKED_REQUEST, $user->ID);
        return_error_page_redirection($blocking_reasons);
        // return ['error' => $blocking_reasons];
    }

    // below this point we know the user is valid, not blocked, and can be logged in

    // remember id_token as it is needed as hint in case we have an
    // end_session_endpoint to perform single log out (SLO)
    //
    // see also wp_logout action above.
    $end_session_endpoint = load_end_session_endpoint($id_token);
    if ($end_session_endpoint) {
        update_user_meta($user->ID, 'healy-openid-connect-logout-meta', [
            'id_token_hint' => $id_token,
            'end_session_endpoint' => $end_session_endpoint,
        ]);
    }

    // updateing various user attributes here, all of these can also be filtered
    update_user_roles($user, $claims, $access_token);
    update_user_capabilities($user, $claims, $access_token);

    // update user meta can be different claims, depening on if this is a
    // log-in or auto-registering user. Because user meta update can not be
    // done before the use exists, we resolve the mapping above and do the
    // actual updating down here.
    update_user_meta_from_claims($user, $claims, $claims_mapping, $access_token);

    $user_login = $user->user_login; // safe the user_login for possible error reporting, when user return is null
    $user = apply_filters('healy-openid-connect-pre-login-user-update', $user, $claims, $access_token);
    if ($user === null) {
        //dbg('user got nuked by "healy-openid-connect-pre-login-user-update" filter');
        //return ['error' => 'user got nuked by OpenId Connect pre-login filter :('];
        return_error_page_redirection([new Error(Error::NUKED_BY_PRE_LOGIN_FILTER, ['user_login' => $user_login])]);
    }

    // very last step before succesful login, we update the password_hash with what we got from the SSO server
    update_password_hash($user->ID, $claims->password_hash ?? null);

    // trigger the actual wordpress login with auth cookies etc.
    do_wordpress_login($user);
}

function get_user_by_identifying_claim(object $claims): ?\WP_User
{
    // Test for user existance now. We search the WP_User in our database by an
    // identifying claim in the claims. Our default is the email field, but
    // before doing the actuall search we call into a filter giving it the
    // opportunity to change the claim attribute to use as well as the search
    // field.
    //
    // Possible values for $wp_field are: id | ID | slug | email | login.
    //
    [$wp_field, $claim_field] = apply_filters('healy-openid-connect-user-identifying-claim', [
        oidc('wp-user-identifying-field', 'email'),
        // the claim field, when not defined, defaults to whatever WordPress field is set
        oidc('claim-user-identifying-field', oidc('wp-user-identifying-field', 'email'))
    ]);
    /*
    dbg([
        'wp_field' => $wp_field,
        'claim_field' => $claim_field,
        "claims->{$claim_field}" => $claims->{$claim_field} ?? null,
        "claims" => $claims
    ]);
     */

    // pull user from database, when exists
    $user = get_user_by($wp_field, $claims->{$claim_field} ?? null);
    if ($user === false) {
        // fix the braindead WordPress api signature. Return null instead of
        // false|bool when the user can't be found.
        $user = null;
    }

    if ($user) {
        // besides the inc, this call is also initing and fetching the user
        // metrics from wordpress user meta
        metrics()->inc(Metrics::IDENTIFIED_REQUEST, $user->ID);

        //  user metrics key Metrics::IDENTIFIED_REQUEST must be set now, so we
        //  can check its value for 1, meaning it would be the first OIDC login
        //  attempt of this new a unique user.
        //
        [$_, $user_metrics] = metrics()->values();
        if (1 === $user_metrics[Metrics::IDENTIFIED_REQUEST]) {
            metrics()->inc(Metrics::UNIQUE_USERS);
        }
    }

    return $user;
}

// write password hash we got from the openid claims into the wordpress
// database. As long as the password hashing on the SSO server and on this
// wordpress instance is compatible this means that even without ever having
// the password, the user can use same password on the SSO server as on the
// WordPress native log-in.
//
// WordPress uses the PHP password_hash function, which is documented here:
// <https://www.php.net/manual/en/function.password-hash.php>
//
function update_password_hash(int $user_id, string $password_hash = null): void
{
    if (empty($password_hash)) {
        dbg('empty password_hash, not updated');
        return;
    }

    dbg('updating user password_hash from SSO claim');
    global $wpdb;
    $wpdb->update($wpdb->users, ['user_pass' => $password_hash, 'user_activation_key' => ''], ['ID' => $user_id]);
    clean_user_cache($user_id);
    //$wpdb->query("UPDATE $wpdb->users SET user_pass = '$password_hash', user_activation_key = '' WHERE user_login = '$user_login'");
    //$wpdb->query("UPDATE $wpdb->users SET user_pass = '$password_hash' WHERE user_login = '$user_login'");
    //wp_set_password('password', 4);
    dbg('user_pass updated');
}


// function return_landing_page_redirection(): void
// {
//     header("Location: " . landing_page_url());
//     die;
//
//     throw new Exception("oops, this is not expected to be happening. ever!");
// }

function landing_page_url(): string
{
    $url = oidc('landing-page');
    $url = apply_filters('healy-openid-connect-landing-page', $url);
    $url || ($url = home_url());

    return $url;
}


function return_error_page_redirection(array $errors = []): void
{
    $url = error_page_redirection_url($errors);
    throw new ErrorRedirectException($url);
    //header("Location: {$url}");
    //die;
}

function error_page_redirection_url(array $errors = []): string
{
    $url = error_page_url();
    $m = metrics();
    foreach ($errors as $error) {
        $url = add_query_arg('e[]', urlencode(json_encode([$error->code(), $error->params()])), $url);
        err((string) $error); // new Error($reason->code(), $reason->params()));
        $m->incError($error);
    }
    return $url;
}

function error_page_url(): string
{
    $url = oidc('error-page');
    $url = apply_filters('healy-openid-connect-error-page', $url);
    $url || ($url = home_url());

    return $url;
}

// return true when the oidc state cookie values matches, meaning we're not
// under attack
function do_oidc_state_cookie_match(string $state = ''): bool
{
    //return !empty($state) && ($state === oidc_cookie()->val);
    $t = csrf_token();
    return ($t !== null) && ($state === $t); // md5(session_id());
}
// .csrf protection

// The full array of possible user data parameters:
//
//  $userdata = array(
//      'ID'                    => 0,    //(int) User ID. If supplied, the user will be updated.
//      'user_pass'             => '',   //(string) The plain-text user password.
//      'user_login'            => '',   //(string) The user's login username.
//      'user_nicename'         => '',   //(string) The URL-friendly user name.
//      'user_url'              => '',   //(string) The user URL.
//      'user_email'            => '',   //(string) The user email address.
//      'display_name'          => '',   //(string) The user's display name. Default is the user's username.
//      'nickname'              => '',   //(string) The user's nickname. Default is the user's username.
//      'first_name'            => '',   //(string) The user's first name. For new users, will be used to build the first part of the user's display name if $display_name is not specified.
//      'last_name'             => '',   //(string) The user's last name. For new users, will be used to build the second part of the user's display name if $display_name is not specified.
//      'description'           => '',   //(string) The user's biographical description.
//      'rich_editing'          => '',   //(string|bool) Whether to enable the rich-editor for the user. False if not empty.
//      'syntax_highlighting'   => '',   //(string|bool) Whether to enable the rich code editor for the user. False if not empty.
//      'comment_shortcuts'     => '',   //(string|bool) Whether to enable comment moderation keyboard shortcuts for the user. Default false.
//      'admin_color'           => '',   //(string) Admin color scheme for the user. Default 'fresh'.
//      'use_ssl'               => '',   //(bool) Whether the user should always access the admin over https. Default false.
//      'user_registered'       => '',   //(string) Date the user registered. Format is 'Y-m-d H:i:s'.
//      'show_admin_bar_front'  => '',   //(string|bool) Whether to display the Admin Bar for the user on the site's front end. Default true.
//      'role'                  => '',   //(string) User's role.
//      'locale'                => '',   //(string) User's locale. Default empty.
//  );
//
// see also: https://developer.wordpress.org/reference/functions/wp_insert_user/
function provision_user_update_parameters(?\WP_User $wp_user, object $claims, string $access_token = null): array
{
    // ensure that email always only land up in database in all lowercase form
    if ($claims->email) {
        $claims->email = strtolower($claims->email);
    }

    $user_params = [
        'user_login' => $claims->username ?? $claims->email ?? null,
        'user_email' => $claims->email ?? null,
    ];

    $user_params = (array) apply_filters(
        'healy-openid-connect-user-update-parameters',
        $user_params,
        $wp_user,
        $claims,
        $access_token,
    );

    return $user_params;
}

//function do_wordpress_login(int $wp_user_id): void
function do_wordpress_login(\WP_User $user): void
{
    $wp_user_id = $user->ID;
    dbg("do wordpress login for user ID: {$wp_user_id}");
    wp_set_current_user($wp_user_id);
    wp_set_auth_cookie($wp_user_id);

    // update sucess metrics
    metrics()->inc(Metrics::SUCCESSFUL_LOGINS, $user->ID);

    //do_action('wp_login', $user->user_login, $user);
    do_action('healy-openid-connect-successful-login', $user);

    dbg("user logged in: {$wp_user_id}");
}
